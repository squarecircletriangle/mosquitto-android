LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
#LOCAL_C_INCLUDES += ./include  $(KERNEL_HEADERS)
LOCAL_C_INCLUDES +=   $(KERNEL_HEADERS) external/openssl/include

LOCAL_SRC_FILES := \
	mosquitto.c \
	logging_mosq.c \
	memory_mosq.c \
	messages_mosq.c \
	net_mosq.c \
	read_handle.c \
	read_handle_client.c \
 	read_handle_shared.c \
 	send_mosq.c \
 	send_client_mosq.c \
 	srv_mosq.c \
 	thread_mosq.c \
 	time_mosq.c \
 	tls_mosq.c \
 	util_mosq.c \
 	will_mosq.c

LOCAL_MODULE    := libmqtt
LOCAL_MODULE_TAGS := optional
LOCAL_CFLAGS += -DVERSION="134" -DTIMESTAMP="20140820"  -DWITH_TLS -DWITH_TLS_PSK 
LOCAL_LDLIBS += -lpthread -ldl

#-DWITH_SRV   -DWITH_BRIDGE -DWITH_THREADING  -DWITH_MEMORY_TRACKING -DWITH_SYS_TREE   -DWITH_BROKER  -DWITH_SRV  -D-fPIC -DWITH_THREADING   -DWITH_THREADING  
# -D-lrt
#LOCAL_CFLAGS += -DVERSION="134" -DTIMESTAMP="20140820" 
include $(BUILD_STATIC_LIBRARY)

